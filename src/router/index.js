import { createRouter, createWebHistory } from "vue-router";
import GetStarted from "../views/GetStarted.vue";
import Status from "../views/Status.vue";
import Results from "../views/Results.vue";

const routes = [
  { path: "/", component: GetStarted },
  { path: "/status", component: Status },
  { path: "/results", component: Results },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
