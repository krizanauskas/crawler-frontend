import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import "./assets/tailwind.css";
import Pusher from "pusher-js";
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

const pusher = new Pusher(process.env.VUE_APP_PUSHER_API_KEY, {
  cluster: process.env.VUE_APP_PUSHER_CLUSTER,
});

const channel = pusher.subscribe("crawling-progress-channel");
channel.bind("pusher:subscription_succeeded", () => console.log("subscription succeeded"));
const app = createApp(App);
app
  .use(router)
  .use(Toast)
  .mount("#app");

app.provide("channel", channel);
